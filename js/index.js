require.config({ paths: { 'vs': 'https://unpkg.com/monaco-editor@0.15.5/min/vs' }});
window.MonacoEnvironment = { getWorkerUrl: () => proxy };

let proxy = URL.createObjectURL(new Blob([`
	self.MonacoEnvironment = {
		baseUrl: 'https://unpkg.com/monaco-editor@0.15.5/min/'
	};
	importScripts('https://unpkg.com/monaco-editor@0.15.5/min/vs/base/worker/workerMain.js');
`], { type: 'text/javascript' }));


 require(["vs/editor/editor.main"], function () {

	var data = monaco.editor.getModel('https://unpkg.com/monaco-editor@0.15.5/min/vs/base/worker/workerMain.js', 'utf-8');

		window.editor = monaco.editor.create(document.getElementById('editor'),{
			value: [
			'data'	
			]
			.join('\n'),
			language: 'yaml',
			theme: 'vs-dark'
		});
	});

/*
 var loadOrg = monaco.editor.getModel('https://unpkg.com/monaco-editor@0.15.5/min/vs/base/worker/workerMain.js');
 var originalModel = monaco.editor.createModel(loadOrg);

 var loadMod = monaco.editor.getModel(fs.readFileSync('vs/loader.js', 'utf-8'));
 var modifiedModel = monaco.editor.createModel(loadMod);
 
 var diffEditor = monaco.editor.createDiffEditor(document.getElementById("editor"), {
	// You can optionally disable the resizing
	enableSplitViewResizing: false,

	// Render the diff inline
	renderSideBySide: true,

	theme: 'vs-dark',
	originalEditable: true,
	});
 diffEditor.setModel({
	 original: originalModel,
	 modified: modifiedModel
 });
});
*/